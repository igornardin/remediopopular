# Create your tasks here
import tweepy
import time
import re
from celery import Celery
from site_teste.models import twitt, farmacia, remedio, valor
from celery import shared_task

consumer_key="cu7maYzHOeGLqCAJG7LkSt1KV"
consumer_secret="jnc2jFgD7bqSpjt9amJ6jmxK34Pxksb5bs3Rm65uLnJMfAs2xt"
access_token_key='971179066912108544-JorjAldHmfxXI061zq3ZrI5qe34zEnh'
access_token_secret='NbBKoUBtbmoKSCxliPNyMEat6LJ1mLXvdckAYzJiIfuhe'

app = Celery('tasks', broker='pyamqp://guest@localhost//')

@shared_task
def twitter():
    print("Tarefa twitter chamada!")
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token_key, access_token_secret)
    api = tweepy.API(auth)
    
    results = api.search(q="#remediopop")
    
    for i in range(len(results)):
        twittid = results[i].id
        twittidstr = results[i].id_str
        username = results[i].user.screen_name
        text = results[i].text
        datatwt = results[i].created_at
        
        print('Twitt encontrado: ' + text)
        
        if(len(twitt.objects.filter(id=twittid)) != 0):
            print('Twitt já existe!')
            continue
        
#Busca Farmácia
        encfar = False
        
        for farm in farmacia.objects.all():
            if (text.find(farm.nome) != -1):
                encfar = True
                break
        
        if(encfar != True):
            continue

#Busca Remédio    
        encrem = False
        
        for remed in remedio.objects.all():
            if (text.find(remed.nome) != -1):
                encrem = True
                break
        
        if(encrem != True):
            continue

        match = re.search(r'([0-9,-]+)', text)
        print(match[0])
        if (match == None):
            continue
            
        twitt.objects.create(id=twittid, idstr=twittidstr, user=username, text=text, farmacia=farm, remedio=remed, data=datatwt, valor_remedio=float(match[0].replace(',', '.')))
        print('Novo twitt adicionado!')
        print(twitt)
    
    
@shared_task
def proctwitt():
    valor.objects.all().delete()
    print("Deletou os valores...")
    for twitts in twitt.objects.all():
        nome_valor = "Farmacia: " + twitts.farmacia.nome + " Remedio: " + twitts.remedio.nome 
        try:
            val = valor.objects.get(farmacia=twitts.farmacia, remedio=twitts.remedio)
            if(val.data < twitts.data or (val.data == twitts.data and val.valor > twitts.valor_remedio)):
                val.valor = twitts.valor_remedio
                val.data = twitts.data
                val.save()
        except valor.DoesNotExist:
            valor.objects.create(nome=nome_valor, farmacia=twitts.farmacia, remedio=twitts.remedio, valor=twitts.valor_remedio, data=twitts.data)
            