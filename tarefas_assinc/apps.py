from __future__ import absolute_import, unicode_literals
from django.apps import AppConfig


class TarefasAssincConfig(AppConfig):
    name = 'tarefas_assinc'
