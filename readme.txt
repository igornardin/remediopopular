1) Instalar os requirements do arquivo requirements.txt

2) Iniciar o worker do broker com o comando:
celery -A remediopopular worker -l info

3) Iniciar o beat do broker com o comando:
celery -A remediopopular beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler

4) Criar um superuser
python manage.py createsuperuser 

5) Inicializar o servidor Python:
python manage.py runserver --insecur

6) Acessar http://127.0.0.1:8000/

7) Incluir uma farmácia em http://127.0.0.1:8000/admin/site_teste/farmacia/

7) Incluir um remédio em http://127.0.0.1:8000/admin/site_teste/remedio/

8) Twittar com a hashtag #remediopop de qualquer usuário com a farmácia e o remédio incluído

9) Acessar a página inicial e verificar os remédios incluídos