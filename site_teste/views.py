from django.shortcuts import render
from .models import *

def remedio_list(request):
    remedios = remedio.objects.all()
    farmacias = farmacia.objects.all()
    valores = valor.objects.all()
    return render(request, 'remedio_list.html', {'remedios': remedios, 'farmacias': farmacias, 'valores': valores})

def post_detail(request, remedio_twitt, farmacia_twitt):
    rem = remedio.objects.get(nome=remedio_twitt)
    far = farmacia.objects.get(nome=farmacia_twitt)
    twitts = twitt.objects.filter(farmacia=far, remedio=rem)
    return render(request, 'post_detail.html', {'twitts': twitts})
