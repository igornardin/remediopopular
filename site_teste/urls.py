from django.urls import path
from . import views

urlpatterns = [
    path('', views.remedio_list, name='remedio_list'),
    path('post/<remedio_twitt>/<farmacia_twitt>/', views.post_detail, name='post_detail'),
]