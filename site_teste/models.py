from django.db import models

#Classe de remédios
class remedio(models.Model):
    id = models.IntegerField(default=0, primary_key=True)
    nome = models.TextField(default='')
 
    def __str__(self):
        return self.nome

#Classe de farmácias
class farmacia(models.Model):
    id = models.IntegerField(default=0, primary_key=True)
    nome = models.TextField(default='')
    cidade = models.TextField(default='')
 
    def __str__(self):
        return self.nome

#Classe de Twitt
class twitt(models.Model):
    id = models.IntegerField(default=0, primary_key=True)
    idstr = models.TextField()
    user = models.TextField()
    text = models.TextField()
    data = models.DateField()
    valor_remedio = models.FloatField()
    farmacia = models.ForeignKey(farmacia, on_delete=models.CASCADE)
    remedio = models.ForeignKey(remedio, on_delete=models.CASCADE)
 
    def __str__(self):
        return self.idstr

#Classe de Valores_remedio
class valor(models.Model):
    nome = models.TextField(default="")
    farmacia = models.ForeignKey(farmacia, on_delete=models.CASCADE)
    remedio = models.ForeignKey(remedio, on_delete=models.CASCADE)
    valor = models.FloatField()
    data = models.DateField(null=True, blank=True)
    def __str__(self):
        return self.nome
